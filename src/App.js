import React, { useState, useEffect } from "react";
import { Button, Input, InputLabel, FormControl } from "@material-ui/core/";
import "./App.css";
import Todo from "./Todo.js";
import db from "./firebase";
import firebase from "firebase";

function App() {
  const [todos, settodos] = useState([]);
  const [input, setinput] = useState([]);
  useEffect(() => {
    db.collection("todos")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) => {
        settodos(snapshot.docs.map((doc) => {id: doc.id, todo: doc.data().todo}))
      })
  }, []);
  const addTodo = (dos) => {
    dos.preventDefault();
    db.collection("todos").add({
      todo: input,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    setinput("");
  };

  return (
    <div className="App">
      <h1> Hello World 🔥 </h1>
      <form>
        <FormControl>
          <InputLabel>✍Add you dos!</InputLabel>
          <Input
            value={input}
            onChange={(event) => setinput(event.target.value)}
          >
            Add you dos!
          </Input>
        </FormControl>
        <Button
          disabled={input < 1}
          onClick={addTodo}
          variant="outlined"
          color="secondary"
        >
          Add Tod ✍
        </Button>
      </form>
      {todos.map((todo) => (
        <Todo todo={todo} id ={id} />
      ))}
    </div>
  );
}

export default App;
