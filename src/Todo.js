import {
  ListItemAvatar,
  List,
  Avatar,
  ListItem,
  ListItemText,
  Button,
  Modal,
  makeStyles,
} from "@material-ui/core";
import { React, ReactFragment, useState } from "react";
import "./Todo.css";
import db from "./firebase";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
function Todo(props) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const [input, setInput] = useState[props.todo];
  const updateTodo = () => {
    db.collection("todos")
      .doc(props.todo.id)
      .set({ todo: input }, { merge: true });
    setOpen(false);
  };

  return (
    <ReactFragment>
      <Modal open={open} onClose={(e) => setOpen(false)}>
        <div>
          <h1>Close</h1>
          <intput
            placeholder={props.todo}
            value={input}
            onChange={(inputed) => setInput(inputed.target.value)}
          />
          <button onClick={(e) => setOpen(false)}></button>
        </div>
      </Modal>

      <List className="todo_list">
        <ListItem>
          <ListItemAvatar>
            <Avatar></Avatar>
          </ListItemAvatar>
          <ListItemText primary="Todo" secondary={props.todo} />
          <Button
            onClick={(event) =>
              db.collection("todos").doc(props.todo.id).delete()
            }
          >
            🚚Delete Me
          </Button>
        </ListItem>
        <button
          onClick={(e) => setOpen(true)}
          type="button"
          onClick={handleOpen}
        >
          Edit Me
        </button>
      </List>
    </ReactFragment>
  );
}

export default Todo;
