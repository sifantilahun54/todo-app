// For Firebase JS SDK v7.20.0 and later, measurementId is optional
/*const firebaseConfig = {
  apiKey: "AIzaSyAqZv7O7dLMc4RMn6ihhEhcQT9wrtPCub8",
  authDomain: "minale-addis.firebaseapp.com",
  databaseURL: "https://minale-addis.firebaseio.com",
  projectId: "minale-addis",
  storageBucket: "minale-addis.appspot.com",
  messagingSenderId: "589318239754",
  appId: "1:589318239754:web:a6379ef607e7034fcd11f3",
  measurementId: "G-F7NBN8KT85",
};
*/
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAqZv7O7dLMc4RMn6ihhEhcQT9wrtPCub8",
  authDomain: "minale-addis.firebaseapp.com",
  databaseURL: "https://minale-addis.firebaseio.com",
  projectId: "minale-addis",
  storageBucket: "minale-addis.appspot.com",
  messagingSenderId: "589318239754",
  appId: "1:589318239754:web:a6379ef607e7034fcd11f3",
  measurementId: "G-F7NBN8KT85",
});
const db = firebaseApp.firestore();

export default db;
